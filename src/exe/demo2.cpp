#include <iostream>
#include <fstream>
#include <random>
#include <base/sample.h>
#include <base/solution.h>
#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <search.h>


int main(int argc, char ** argv) {
    std::mt19937 gen1( atoi(argv[1]) );
    Sample sampleTrain(argv[2]);
    Sample sampleTest(argv[3]);
    Solution<double> solution(8,2);
    SmartEval smartEval(gen1, sampleTrain);
    SmartEval smartEval2(gen1, sampleTest);
    HillClimber hillclimber(10, smartEval, gen1);

    /*region hillclimber*/
//    hillclimber.run(solution);
//    double trainFitness =  solution.fitness();
//    smartEval2(solution);
//
//    std::cout<<trainFitness<< " "<<solution.fitness()<<std::endl;


    /*region ils*/
    IteratedLocalSearch ils(100, gen1);
    ils.run(solution, hillclimber);
    std::cout<<ils.bestSolution.fitness()<<" ";
    HillClimber hillclimber2(10, smartEval2, gen1);
    ils.run(solution, hillclimber2);
    std::cout<<ils.bestSolution.fitness()<<std::endl;



    return 0;
}
