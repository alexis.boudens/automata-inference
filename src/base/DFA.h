/*
  DFA.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/04/08 : Version 0
*/

#ifndef _DFA_h
#define _DFA_h

#include <iostream>
#include <fstream>
#include <vector>

#include "rapidjson/document.h"

#include <base/word.h>

/*
    Deterministic Finite Automaton (DFA)
*/
class DFA {
public:
    /*
        Constructor 
        Empty language: no word is accepted
    */
    DFA(unsigned _nStates, unsigned _alphabetSize) : nStates(_nStates), alphabetSize(_alphabetSize) {
        if (nStates == 0)
            std::cerr << "number of states must be strictly greater than 0" << std::endl;

        startState = 0;

        acceptStates.resize(nStates, false);

        function.resize(nStates);
        for(unsigned i = 0; i < nStates; i++) {
            function[i].resize(alphabetSize, -1);
        }
    }

    /*
        Empty language: no word is accepted
    */
    DFA() : DFA(1, 1) {
    }

    /*
        Copy Constructor 
    */
    DFA(const DFA & _source) : nStates(_source.nStates), alphabetSize(_source.alphabetSize), startState(_source.startState) {
        acceptStates.resize(nStates);
        for(unsigned i = 0; i < nStates; i++) 
            acceptStates[i] = _source.acceptStates[i];

        function.resize(nStates);
        for(unsigned i = 0; i < nStates; i++) {
            function[i].resize(alphabetSize);
            for(unsigned j = 0; j < alphabetSize; j++) 
                function[i][j] = _source.function[i][j];
        }
    }

    /*
        Constructor 
        Read from file in json format
    */
    DFA(const char * _instance_fileName) {
        std::ifstream filein(_instance_fileName);

        if (!filein)
            std::cerr << "Impossible to open " << _instance_fileName << std::endl;

        readFrom(filein);

        filein.close();
        
    }

    DFA& operator=(const DFA & _source) {
        nStates = _source.nStates;
        alphabetSize = _source.alphabetSize;
        startState = _source.startState;

        acceptStates.resize(nStates);
        for(unsigned i = 0; i < nStates; i++) 
            acceptStates[i] = _source.acceptStates[i];

        function.resize(nStates);
        for(unsigned i = 0; i < nStates; i++) {
            function[i].resize(alphabetSize);
            for(unsigned j = 0; j < alphabetSize; j++) 
                function[i][j] = _source.function[i][j];
        }

        return *this;
    }

    /*
        Read a word with the automata
        -1 : no transition, the word is rejected
    */
    int read(Word & w) {
        int state = startState;
        unsigned i = 0;

        while (0 <= state && i < w.s.size()) {
            state = function[state][ w.s[i] ];
            i++;
        }

        return state;
    }

    /*
        Read a word with the automata
        -1 : no transition, the word is rejected
    */
    int read(Word & w, std::vector<unsigned> & usedStates) {
        int state = startState;
        unsigned i = 0;

        usedStates[state]++;
        while (0 <= state && i < w.s.size()) {
            state = function[state][ w.s[i] ];
            usedStates[state]++;
            i++;
        }

        return state;
    }

    /*
        Read and accept (or not) the word
        -1 : no transition, the word is rejected
    */
    bool accept(Word & w) {
        int state = read(w);

        if (state < 0)
            return false;
        else
            return acceptStates[state];
    }

    /*
        Read and accept (or not) the word
        -1 : no transition, the word is rejected
    */
    bool accept(Word & w, std::vector<unsigned> & usedStates) {
        int state = read(w, usedStates);

        if (state < 0)
            return false;
        else
            return acceptStates[state];
    }

    /*
        Output
    */
    virtual void printOn(std::ostream& _os) const {
        _os << "{" ;

        _os << "\"nStates\":" << nStates << "," ;

        _os << "\"alphabetSize\":" << alphabetSize << ","  ;

        _os << "\"startState\":" << startState << "," ;

        _os << "\"acceptStates\":[" ;
        for(unsigned i = 0; i < acceptStates.size(); i++) {
            if (i > 0) _os << "," ;
            if (acceptStates[i])
                _os <<  "true";
            else
                _os <<  "false";
        }
        _os << "]," ;

        _os << "\"function\":[" ;
        for(unsigned i = 0; i < function.size(); i++) {
            if (i > 0) _os << "," ;
            _os << "[" ;
            for(unsigned j = 0; j < function[i].size(); j++) {
                if (j > 0) _os << "," ;
                _os << function[i][j] ;
            }
            _os << "]" ;
        }
        _os << "]" ;

        _os << "}" ;
    }

    /*
        Input
    */
    virtual void readFrom(rapidjson::Value& document) {
        if (document.IsObject()) {
            if (document.HasMember("nStates")) {
                const rapidjson::Value& a = document["nStates"];
                nStates = a.GetInt();

                startState = 0;
            }  else
                std::cerr << "nStates not found." << std::endl;

            if (document.HasMember("alphabetSize")) {
                const rapidjson::Value& a = document["alphabetSize"];
                alphabetSize = a.GetInt();
            }  else
                std::cerr << "alphabetSize not found." << std::endl;

            if (document.HasMember("acceptStates")) {
                acceptStates.resize(0);
                for(const auto& state : document["acceptStates"].GetArray()){
                    acceptStates.push_back( state.GetBool() );
                }

                if (acceptStates.size() != nStates)
                    std::cerr << "Size of acceptStates is not equal to number of states." << std::endl;
            }  else
                std::cerr << "acceptStates not found." << std::endl;

            if (document.HasMember("function")) {
                int ns;
                function.resize(0);
                for(const auto& rules : document["function"].GetArray()){
                    std::vector<int> trans;
                    for(const auto& r : rules.GetArray()) {
                        ns = r.GetInt() ;
                        if (ns < (int) nStates)
                            trans.push_back( ns );
                        else 
                            std::cerr << "Error: non existing state in function: " << ns << std::endl;
                    }

                    if (trans.size() != alphabetSize)
                        std::cerr << "Error: function size is not equal to alphabet size." << std::endl;

                    function.push_back(trans);
                }

                if (function.size() != nStates)
                    std::cerr << "Error: function size is not equal to the number of states." << std::endl;

            }  else
                std::cerr << "function not found." << std::endl;

        } else {
            std::cerr << "DFA: Impossible to parse the json file." << std::endl;            
        }
    }

    virtual void readFrom(std::istream& _is) {
        std::string json((std::istreambuf_iterator<char>(_is)),
                 std::istreambuf_iterator<char>());

        rapidjson::Document document;

        document.Parse(json.c_str());

        if (document.IsObject()) {
            readFrom(document);
        } else {
            std::cerr << "DFA::readFrom: Impossible to parse the json istream " << std::endl;            
        }
    }


    // number of states
    unsigned nStates;

    // size of the alphabet
    unsigned alphabetSize;

    // transition function
    std::vector< std::vector<int> > function;

    // start state
    int startState;

    // accept states
    std::vector<bool> acceptStates;

};

std::ostream & operator<<(std::ostream& _os, const DFA& _s) {
    _s.printOn(_os);
    return _os;
}

std::istream & operator >> (std::istream& _is, DFA& _s) {
  _s.readFrom(_is);
  return _is;
}


#endif
