/*
 solution.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
*/

#ifndef _solution_h
#define _solution_h

#include <iostream>
#include <vector>
#include "rapidjson/document.h"

#include <base/DFA.h>

// usefull for print, but maybe not at the correct place
std::ostream & operator<<(std::ostream& _os, const std::pair<double, unsigned> & _f) {
    _os << "[" << _f.first << "," << _f.second << "]" ;
    return _os;
}

/*
  Candidate solution of automata inference problem
*/
template< class Fitness >
class Solution : public DFA {
public:
  using DFA::readFrom;

  Solution() : DFA() {
    valid = false;
  }

  Solution(unsigned _nStates, unsigned _alphabetSize) : DFA(_nStates, _alphabetSize) {
    valid = false;
  }

  Solution(const Solution & _s) : DFA(_s) {
    _fitness = _s.fitness();
    valid = _s.isValid();
  }

  Solution& operator=(const Solution & _s) {
    // should be replaced by DFA::operator= ....
    nStates = _s.nStates;
    alphabetSize = _s.alphabetSize;
    startState = _s.startState;

    acceptStates.resize(nStates);
    for(unsigned i = 0; i < nStates; i++) 
        acceptStates[i] = _s.acceptStates[i];

    function.resize(nStates);
    for(unsigned i = 0; i < nStates; i++) {
        function[i].resize(alphabetSize);
        for(unsigned j = 0; j < alphabetSize; j++) 
            function[i][j] = _s.function[i][j];
    }

    _fitness = _s.fitness();
    valid = _s.isValid();

    return *this;
  }

    void setRandom(std::mt19937& rand){
        std::uniform_real_distribution<double> dis(0, 1);
        for(int i=0; i<nStates; i++){
            for(int j=0; j<alphabetSize; j++){
                function[i][j] = dis(rand)*(nStates);
            }
        }
    }

    std::vector<Solution<Fitness>> getNeighbors(){
        std::vector<Solution<Fitness>> neighbors;

        Solution<Fitness> neighborSolution;

        for(int i=0; i<nStates; i++){
            for(int j=0; j<alphabetSize; j++){
                for(int k=0; k<nStates; k++){
                    neighborSolution = *this;
                    neighborSolution.function[i][j]=k;
                    neighbors.push_back(neighborSolution);
                }
            }
        }
        return neighbors;
    }

  /**
   * set the fitness
   */
  void fitness(Fitness _fit) {
    valid    = true;
    _fitness = _fit;
  }

  /**
   * get the fitness
   */
  Fitness fitness() const { 
    return _fitness; 
  }

  /**
   * print the solution
   */
  void print() {
      printOn(std::cout);
  }

  virtual void printOn(std::ostream& _os) const {
    _os << "{" ;

    _os << "\"v\":" ;

    if (valid)
      _os << "true," ;
    else
      _os << "false," ;

    _os << "\"f\":" << this->fitness() << "," ;

    _os << "\"dfa\":" << ((DFA) *this) ;

    _os << "}" ;
  }

  virtual void readFrom(std::istream& _is) {
    rapidjson::Document document;

    std::string json((std::istreambuf_iterator<char>(_is)),
                 std::istreambuf_iterator<char>());

    document.Parse(json.c_str());

    if (document.IsObject()) {
        if (document.HasMember("v")) {
            const rapidjson::Value& a = document["v"];
            valid = a.GetBool();

        }  else
            std::cerr << "\"v\" not found." << std::endl;

        if (document.HasMember("f")) {
          /*
            const rapidjson::Value& a = document["f"];
            _fitness = (Fitness) a.GetDouble();
          */
          readFitness(document, _fitness);

        }  else
            std::cerr << "\"f\" not found." << std::endl;

        if (document.HasMember("dfa")) {
          //((DFA) *this).readFrom(document["dfa"]);
          readFrom(document["dfa"]);
        }  else
            std::cerr << "\"dfa\" not found." << std::endl;
    }
  }

  void invalidate() {
    valid = false;
  }

  bool isValid() const {
    return valid;
  }

protected:
  void readFitness(rapidjson::Value& document, double & _fit) {
      const rapidjson::Value& a = document["f"];
      _fit = (Fitness) a.GetDouble();    
  }

  void readFitness(rapidjson::Value& document, std::pair<double, unsigned> & _fit) {
      const rapidjson::Value& a = document["f"].GetArray();
      _fit.first = a[0].GetDouble();
      _fit.second = a[1].GetDouble();
  }


  // quality of the solution
  Fitness _fitness;

  // valid fitness value if true
  bool valid;
};

template< class Fitness >
std::ostream & operator<<(std::ostream& _os, const Solution<Fitness>& _s) {
    _s.printOn(_os);
    return _os;
}

template< class Fitness >
std::istream & operator >> (std::istream& _is, Solution<Fitness>& _s) {
  _s.readFrom(_is);
  return _is;
}

#endif
