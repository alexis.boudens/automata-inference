//
// Created by alexis on 15/10/2019.
//

#ifndef AUTOMATA_INFERENCE_SEARCH_H
#define AUTOMATA_INFERENCE_SEARCH_H

#include "eval/eval.h"
#include "eval/basicEval.h"
#include "base/solution.h"
#include "base/DFA.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <random>

class LocalSearch
{
    public:
        virtual void run(Solution<double>& solution)=0;
};


class HillClimber : public LocalSearch{
    private:
        int _nbEval;
        Eval<double>& _eval;
        std::mt19937 _random;

    public :
        HillClimber(int nbEval, Eval<double>& eval, std::mt19937 random) : LocalSearch(), _nbEval(nbEval), _eval(eval),  _random(random)
        {}

        void run(Solution<double> & solution){
            _eval(solution);
            Solution<double> bestSolution(solution);
            int iEval = 0;
            while(iEval < _nbEval){
                std::vector<Solution<double>> neighbors = solution.getNeighbors();
                solution.setRandom(_random);
                for(int i=0; i<neighbors.size(); i++){
                    _eval(neighbors[i]);
                    if(solution.fitness()<neighbors[i].fitness()){
                        bestSolution=neighbors[i];
                    }
                }
                iEval++;
            }
            solution = bestSolution;
        }
};

class IteratedLocalSearch {
public:
    int _nbEval;
    std::mt19937 _random;
    Solution<double> bestSolution;

public :
    IteratedLocalSearch(int nbEval, std::mt19937 random) :  _nbEval(nbEval),  _random(random)
    {}

    void run(Solution<double> & solution, HillClimber hillClimber){
        hillClimber.run(solution);
        bestSolution = solution;
        int iEval = 0;
        while(iEval < _nbEval) {
            //création d'une solution random pour perturbation
            Solution<double>secondSolution (solution.nStates, 2);
            std::uniform_int_distribution<int>rand(0,secondSolution.nStates - 1);
            for(int i = 0; i<secondSolution.nStates; i++){
                for(int j = 0; j< 2; j++){
                    secondSolution.function[i][j] = rand(_random);
                }
            }

            hillClimber.run(secondSolution);
            if(bestSolution.fitness() < secondSolution.fitness() ) {
                bestSolution = secondSolution;
            }
            iEval++;
        }
    }
};

#endif //AUTOMATA_INFERENCE_SEARCH_H
