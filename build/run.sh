#!/bin/bash

# nombre d'exécutions
nbRun=50

# Hil-Climbing
echo hill-climbing
> result.csv

for INSTANCE in {0..29}
do
	for((i=0; i < ${nbRun};i++))
	do
	    echo -n $i' '
	    ./demo2 $i ../instances/dfa_8_"$INSTANCE"_0.05_train-sample.json ../instances/dfa_8_"$INSTANCE"_0.05_test-sample.json >> result_005.csv
	done
done


