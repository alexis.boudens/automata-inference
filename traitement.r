dun <- read.table("/home/clement/Bureau/Results/result_01.csv",header = TRUE, sep=" ")
dtrois <- read.table("/home/clement/Bureau/Results/result_001.csv",header = TRUE, sep=" ")
ddeux <- read.table("/home/clement/Bureau/Results/result_005.csv",header = TRUE, sep=" ")

names(dun) <- c("train", "test")
names(ddeux) <- c("train", "test")
names(dtrois) <- c("train", "test")


hist(dun$test)
hist(dun$train)
hist(ddeux$test)
hist(ddeux$train)
hist(dtrois$test)
hist(dtrois$train)
boxplot(dun$train,dun$test,ddeux$train,ddeux$test,dtrois$train,dtrois$test,names=c(0.1,0.1,0.05,0.05,0.01,0.01),col=c("blue","red","blue","red","blue","red"))
